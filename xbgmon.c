#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/select.h>
#include <time.h>
#include <unistd.h>
#include <X11/Xatom.h>
#include <X11/Xft/Xft.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

typedef struct CPU {
	 int usr,  nc,  sys,  idl,  iow,  irq,  srq,  stl,  gst,  gn;
} CPU;

float calccpu(CPU a, CPU b) {
	int atot = a.usr + a.nc + a.sys + a.idl + a.iow + a.irq + a.srq + a.gst + a.gn;
	int btot = b.usr + b.nc + b.sys + b.idl + b.iow + b.irq + b.srq + b.gst + b.gn;
	int absy = atot - a.idl - a.iow;
	int bbsy = btot - b.idl - b.iow;
	int tdiff = btot - atot;
	int bdiff = bbsy - absy;
	float pct = ( (float)bdiff / (float)tdiff ) * 100.0;
	return pct;
}

CPU getcpu() {
	CPU cpu;
	FILE *f = fopen("/proc/stat", "r");
	fscanf(f, "cpu %d %d %d %d %d %d %d %d %d %d", &cpu.usr, &cpu.nc, &cpu.sys, &cpu.idl, &cpu.iow, &cpu.irq, &cpu.srq, &cpu.stl, &cpu.gst, &cpu.gn);
	fclose(f);
	return cpu;
}

float getmem() {
	int tot, avb, use;
	FILE *f = fopen("/proc/meminfo", "r");
	fscanf(f, "MemTotal: %d kB\nMemFree: %*d kB\nMemAvailable: %d kB", &tot, &avb);
	fclose(f);
	use = tot - avb;
	float mem = (float)use / 1024.0 / 1024.0 ;
	return mem;
}

int gettemp() {
	int temp;
	FILE *f = fopen("/sys/class/hwmon/hwmon2/temp1_input", "r");
	fscanf(f, "%d", &temp);
	fclose(f);
	temp /= 1000;
	return temp;
}


void add_ewmh(Display* display, Window window, Atom state, Atom atom1, Atom atom2) {
	XEvent e;

	memset(&e, 0, sizeof(e));
	e.xclient.type = ClientMessage;
	e.xclient.message_type = state;
	e.xclient.display = display;
	e.xclient.window = window;
	e.xclient.format = 32;
	e.xclient.data.l[0] = 1 /* _NET_WM_STATE_ADD */ ;
	e.xclient.data.l[1] = atom1;
	e.xclient.data.l[2] = atom2;
	e.xclient.data.l[3] = 0l;
	e.xclient.data.l[4] = 0l;

	XSendEvent(display, DefaultRootWindow(display), False,
			   SubstructureRedirectMask, &e);
}

void draw_shadow(Display* display, XftDraw* draw, XftFont* font, XftColor* col, int x, int y, char* buf) {
	//Calculate offsets
	int xa = x + 1;
	int ya = y - 1;
	int xo = 2;
	int yo = 2;
	int xb[] = { xa + xo, xa - xo };
	int yb[] = { ya + yo, ya - yo };
	int xc[] = {
		xb[0] + xo/2,
		xb[0] - xo/2,
		xb[1] + xo/2,
		xb[1] - xo/2
	};
	int yc[] = {
		yb[0] + yo/2,
		yb[0] - yo/2,
		yb[1] + yo/2,
		yb[1] - yo/2
	};

	//Bottom layer
	//XSetForeground(display, ctx, 0x10000000);
	col->color.alpha = 0x0a00;
	for (int xi = 0; xi <= 3; xi++)
	for (int yi = 0; yi <= 3; yi++)
		//XDrawString(display, window, ctx, xc[xi], yc[yi], buf, 16);
		XftDrawStringUtf8(draw, col, font, xc[xi], yc[yi], (FcChar8 *) buf, 20);

	// Mid
	//XSetForeground(display, ctx, 0x22000000);
	col->color.alpha = 0x1a00;
	for (int xi = 0; xi <= 1; xi++)
	for (int yi = 0; yi <= 1; yi++)
		//XDrawString(display, window, ctx, xb[xi], yb[yi], buf, 16);
		XftDrawStringUtf8(draw, col, font, xb[xi], yb[yi], (FcChar8 *) buf, 20);

	//Main
	//XSetForeground(display, ctx, 0xaa000000);
	col->color.alpha = 0x8800;
	//XDrawString(display, window, ctx, xa, ya, buf, 16);
	XftDrawStringUtf8(draw, col, font, xa, ya, (FcChar8 *) buf, 20);
}

int main(void) {	
	CPU prev = getcpu();

	// Window setup
	Display *display = XOpenDisplay(NULL);
	assert(display);

	int fd = ConnectionNumber(display);

	XVisualInfo vinfo;
	XMatchVisualInfo(display, DefaultScreen(display), 32, TrueColor, &vinfo);

	XSetWindowAttributes attribs;
	attribs.colormap = XCreateColormap(display, DefaultRootWindow(display), vinfo.visual, AllocNone);
	attribs.border_pixel = 0;
	attribs.background_pixel = 0;

	Window window = XCreateWindow(display, DefaultRootWindow(display), 0, 0, 350, 75, 0,
		vinfo.depth, CopyFromParent, vinfo.visual,
		CWColormap | CWBorderPixel | CWBackPixel, &attribs);
	XStoreName(display, window, "xbgmon");
	XClassHint *class = XAllocClassHint();
	class->res_name = "xbgmon";
	class->res_class = "xbgmon";
	XSetClassHint(display, window, class);
	//XFree(class);

	// EWMH Atoms, position and such
	Atom window_type = XInternAtom(display, "_NET_WM_WINDOW_TYPE", False);
	long dock = XInternAtom(display, "_NET_WM_WINDOW_TYPE_DOCK", False);
	XChangeProperty(display, window, window_type, XA_ATOM, 32, PropModeReplace, (unsigned char *) &dock, 1);

	XMapWindow(display, window);
	XMoveWindow(display, window, 1135, 12);
	XLowerWindow(display, window);
		
	Atom net_wm_state = XInternAtom(display, "_NET_WM_STATE", False);
	Atom net_wm_below= XInternAtom(display, "_NET_WM_STATE_BELOW", False);
	Atom net_wm_sticky= XInternAtom(display, "_NET_WM_STATE_STICKY", False);
	Atom net_wm_pager= XInternAtom(display, "_NET_WM_STATE_SKIP_PAGER", False);
	Atom net_wm_taskbar= XInternAtom(display, "_NET_WM_STATE_SKIP_TASKBAR", False);
	add_ewmh(display, window, net_wm_state, net_wm_below, net_wm_sticky);
	add_ewmh(display, window, net_wm_state, net_wm_pager, net_wm_taskbar);

	// Font setup
	//Font font = XLoadFont(display, "-misc-config rounded-medium-r-normal--0-230-0-0-p-0-*-*"); 

	//GC ctx = XCreateGC(display, window, 0, NULL);
	//XSetFont(display, ctx, font);

	XftDraw *draw = XftDrawCreate(display, window, vinfo.visual, attribs.colormap);
	XftColor white, black;
	XftFont *font;

	font = XftFontOpen(display, 0, 
		XFT_FAMILY, XftTypeString, "Config Rounded Medium",
		XFT_SIZE, XftTypeDouble, 24.0,
		NULL);
	//XRenderColor wht = { 0xffff, 0xffff, 0xffff, 0xffff };
	XftColorAllocName(display, vinfo.visual, attribs.colormap, "white", &white);
	XftColorAllocName(display, vinfo.visual, attribs.colormap, "black", &black);

	sleep(1);
	// Loop
	for (;;) {
		// Get values 
		int temp;
		float cpu, mem;
		CPU cur = getcpu();
		cpu = calccpu(prev, cur);
		mem = getmem();
		temp = gettemp();

		char buf[32];
		snprintf(buf, 32, "%d°C | %.1f%% | %.2fG", temp, cpu, mem);
		prev = cur;
		// Display
		XClearWindow(display, window);
		//XSetForeground(display, ctx, 0x88000000);
		//XDrawString(display, window, ctx, 2, 26, buf, 16);
		draw_shadow(display, draw, font, &black, 11, 38, buf);
		//XSetForeground(display, ctx, 0xffffffff);
		//XDrawString(display, window, ctx, 10, 34, buf, 16);
		XftDrawStringUtf8(draw, &white, font, 10, 34, ( FcChar8 * ) buf, 20);

		// I have no clue what the fuck this does
		fd_set set;
		FD_ZERO(&set);
		FD_SET(fd, &set);
		select(fd + 1, &set, NULL, NULL, &(struct timeval) {1, 0});

		XEvent event;
		while (XPending(display))
			XNextEvent(display, &event);

		sleep(20);
	}
}
