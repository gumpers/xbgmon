A clone of xbgclock that displays cpu temp, used cpu % and used ram instead.
(This is almost guaranteed to crash on any computer other than mine due to the sensor path being hardcoded in. If you want to use it figure out where your sensor is and edit the code.)
